package tests;

import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;
import pages.Experiance;

public class TestExperiance extends TestBase
{
    HomePage HomeObj;
    LoginPage LogObj;
    Experiance ExperianceObj;
    String Email = "aa111@aa.com";
    String Password = "a1111111";
    String Title = "Tester";
    String Company = "Jobzella";
    String Location="Alex";
    String Description="Hello Alex";


    @Test(priority = 1)
    public void UserCanLoginSeccessfully()
    {

        HomeObj = new HomePage(driver);
        HomeObj.OpenLoginpage();
        LogObj = new LoginPage(driver);
        LogObj.UserLoginPage(Email, Password);

    }

    @Test(priority = 2)
    public void useraddExperiance() throws InterruptedException
    {
        ExperianceObj = new Experiance(driver);
        ExperianceObj.UserAddExperiance(Title,Company,Location,Description);

    }

}
