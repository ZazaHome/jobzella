package tests;

import org.testng.annotations.Test;
import pages.Education;
import pages.HomePage;
import pages.LoginPage;

public class TestEducation extends TestBase
{

    HomePage HomeObj;
    LoginPage LogObj;
    Education EducObj;
    String Email = "aa111@aa.com";
    String Password = "a1111111";
    String School = "BBC";
    String degree ="good";
    String grade  = "V.good";
    String Filedofstudy = "Information Technology";

    @Test(priority = 1)
    public void UserCanLoginSeccessfully()
    {
        HomeObj = new HomePage(driver);
        HomeObj.OpenLoginpage();
        LogObj = new LoginPage(driver);
        LogObj.UserLoginPage(Email, Password);

    }

    @Test(priority = 2)
    public void  UserAddEducation() throws InterruptedException
    {
        EducObj = new Education(driver);
        EducObj.Scroll();
        Thread.sleep(7000);
        EducObj.UserEducation(School,degree,grade,Filedofstudy);
    }
}
