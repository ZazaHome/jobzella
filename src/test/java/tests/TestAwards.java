package tests;

import org.testng.annotations.Test;
import pages.Award;
import pages.HomePage;
import pages.LoginPage;

public class TestAwards extends TestBase

{
    HomePage homObj;
    LoginPage logoObj;
    Award awardsObj;

    String Email = "aa111@aa.com";
    String Password = "a1111111";
    String Title="Hello";
    String Issue="Jobzella";
    String Description="Selenuim Java";


    @Test(priority =1)
    public void UserLogin()
    {
        homObj = new HomePage(driver);
        homObj.OpenLoginpage();
        logoObj = new LoginPage(driver);
        logoObj.UserLoginPage(Email, Password);

    }

    @Test(priority =2)
    public void UserAddAwards() throws InterruptedException
    {
        awardsObj=new Award(driver);
        awardsObj.UserAddAwards(Title,Issue,Description);

    }
}
