package tests;

import org.testng.annotations.Test;
import pages.EditContactInformation;
import pages.HomePage;
import pages.LoginPage;

public class TestEditContactInformation extends TestBase
        {
    HomePage homeObj;
    LoginPage logObj;
    EditContactInformation contactinfoObj;
    String Email = "aa111@aa.com";
    String Password = "a1111111";

    String Mobile = "01003660000";
    String Website = "https://www.google.com.eg/";
    String Facebook = "https://www.facebook.com/";
    String Twitter = "https://twitter.com";
    String Linkedin = "https://www.linkedin.com/";

    @Test(priority = 1)
    public void UserCanlogin()
    {
        homeObj = new HomePage(driver);
        homeObj.OpenLoginpage();
        logObj = new LoginPage(driver);
        logObj.UserLoginPage(Email, Password);

    }

    @Test (priority = 2)
    public void UserContactInfo()
    {
        contactinfoObj = new EditContactInformation(driver);
        contactinfoObj.ContactInfo(Mobile,Website,Facebook,Twitter,Linkedin);
    }
}
