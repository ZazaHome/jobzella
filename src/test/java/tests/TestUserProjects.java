package tests;

import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;
import pages.Projects;

public class TestUserProjects extends TestBase
{

    HomePage HomeObj;
    LoginPage LogObj;
    Projects ProjObj;
    String Email = "aa111@aa.com";
    String Password = "a1111111";
    String Projname = "Jobzella";
    String ProjURL ="http://jbz.la";
    String ProjDescription = "Selenuim by Java";

    @Test(priority=1)
    public void UserCanLoginSeccessfully()
    {

        HomeObj = new HomePage(driver);
        HomeObj.OpenLoginpage();
        LogObj = new LoginPage(driver);
        LogObj.UserLoginPage(Email, Password);
    }
    @Test(priority =2)
    public void UserCanAddProject()
    {
        ProjObj = new Projects(driver);
        ProjObj.UserAddProject(Projname,ProjURL,ProjDescription);


    }

}
