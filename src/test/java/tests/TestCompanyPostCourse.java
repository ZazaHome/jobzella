package tests;

import org.testng.annotations.Test;
import pages.CompanyPostCourse;

import pages.LoginPage;

public class TestCompanyPostCourse extends TestBase
{
    LoginPage LogObj;
    CompanyPostCourse CoursePost;
    String Email = "zaza888@live.com";
    String Password = "12345678A";
    String Languages = "Russian";
    String Map = "Cairo, Egypt";
    String Overload = "2";
    String Price = "2000";
    String Discount = "10";

    @Test(priority = 1)
    public void UserCanLoginSeccessfully()
    {

        LogObj = new LoginPage(driver);
        LogObj.UserLoginPage(Email, Password);

    }

    @Test(priority = 2)
    public void UserAddEducation() throws InterruptedException
    {
        CoursePost = new CompanyPostCourse(driver);
        CoursePost.CoursePosting(Languages, Map, Overload, Price, Discount);
        Thread.sleep(7000);
    }
}
