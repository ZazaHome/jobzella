package tests;

import java.util.concurrent.TimeUnit;
import Utilites.Helper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestResult;
import org.testng.annotations.*;

public class TestBase

{
    public static WebDriver driver;

    @BeforeSuite
    public void startdriver()
    {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/Drivers/chromedriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
        driver.get("http://jbz.la/");

    }

    @AfterClass
    public void cleanup()
    {
        driver.manage().deleteAllCookies();
    }

    @AfterSuite
    public void stopdriver()
    {
        driver.quit();
    }

    @AfterMethod
    public void  ScreenshotonFailuer(ITestResult result)
    {
        if (result.getStatus() == ITestResult.FAILURE)
        {
            System.out.println(" Loading for taking Photo ...! ");
            Helper.CaptureScreenshot(driver ,result.getName() );
        }

    }

}
