package tests;

import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;
import pages.TargetJobs;

public class TestTargetJob extends TestBase
{
    HomePage HomeeObj;
    LoginPage  LoggObj;
    TargetJobs targetObj;
    String Email ="aa111@aa.com";
    String  Password ="a1111111";
    String TargetTitle = "Software Tester";
    String TargetExpectSallary = "2000";

    @Test
    public void UserCanLoggSuccessfully() throws InterruptedException
    {

     HomeeObj = new HomePage(driver);
     HomeeObj.OpenLoginpage();
     LoggObj = new LoginPage(driver);
     LoggObj.UserLoginPage(Email,Password);
     targetObj = new TargetJobs(driver);
     targetObj.TargetJobs(TargetTitle,TargetExpectSallary);
     Thread.sleep(500);
    }
}
