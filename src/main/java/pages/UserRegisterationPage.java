package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;


public class UserRegisterationPage extends PageBase
{
    public UserRegisterationPage(WebDriver driver)
    {
        super(driver);
        action = new Actions(driver);

    }

    @FindBy(id = "firstName")
    WebElement FnameTXT;

    @FindBy(css = "[aria-label='Last Name']")
    WebElement LnameTXT;

    @FindBy(css = "[aria-label='E-mail']")
    WebElement EmailTXT;

    @FindBy(css = "[aria-label='Password']")
    WebElement passwordTXT;

    @FindBy(css = "[aria-label='Confirm Password']")
    WebElement confirmpassword;

    @FindBy(css = "[aria-label='Date of Birth']")
    WebElement Calender;

    @FindBy(xpath = "/html/body/div[1]/div[2]/div/div[1]/div/div/div/ul/li[5]")
    WebElement Select1987;

    @FindBy(xpath = "/html/body/div[1]/div[2]/div/div[1]/div/div/div/div[2]/table/tbody/tr[2]/td[2]")
    WebElement SelectJAN;

    @FindBy(xpath = "/html/body/div[1]/div[2]/div/div[1]/div/div/div/div[2]/table/tbody/tr[2]/td[5]/button")
     WebElement choose6;

    @FindBy(css = ".v-input--selection-controls__ripple")
    WebElement AgreePolicy;

    @FindBy(css = ".v-btn--large .v-btn__content")
    WebElement CreatAccountButton;

    @FindBy(id = "authTopHeaderSettings")
    WebElement userlogoutDropDown;

    @FindBy(id = "authTopHeaderLogout")
    WebElement logout;


    public void UserRegisteration(String Firstname, String Secoundname, String Email, String Password) throws InterruptedException
    {

        SetElement(FnameTXT, Firstname);

        SetElement(LnameTXT, Secoundname);

        SetElement(EmailTXT, Email);

        SetElement(passwordTXT, Password);

        SetElement(confirmpassword, Password);
        Thread.sleep(1000);
        clickOn(Calender);
        Thread.sleep(1000);
        clickOn(Select1987);
        Thread.sleep(1000);
        clickOn(SelectJAN);
        Thread.sleep(1000);
        clickOn(choose6);

        clickOn(AgreePolicy);

        clickOn(CreatAccountButton);

    }

    public void Logout() {
        clickOn(userlogoutDropDown);

        clickOn(logout);


    }
}
