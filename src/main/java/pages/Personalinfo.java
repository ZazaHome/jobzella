package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Personalinfo extends PageBase
{
    public Personalinfo(WebDriver driver)
    {
        super(driver);
    }

 //   @FindBy(id = "personalInfo_GenderInput")
    WebElement GenderSelect;

    @FindBy(xpath = "/html/body/div[1]/div[2]/div/div[43]/div/div/div[1]/a/div")
    WebElement Female;

  //  @FindBy("personalInfo_NumOfDependantsInput")
    WebElement MilitaryStatus;

    @FindBy(xpath = "/html/body/div[1]/div[2]/div/div[41]/div/div/div[3]/a/div")
    WebElement Completed;

  //  @FindBy("personalInfo_NationalityInput")
    WebElement Nationality;

    @FindBy(xpath = "/html/body/div[1]/div[2]/div/div[40]/div/div/div[2]/a/div")
    WebElement Egyptian;

    @FindBy(xpath = "/html/body/div[1]/div[2]/div/div[39]/div/div/div[1]/a/div")
    WebElement Citizen;

    @FindBy(xpath = "/html/body/div[1]/div[2]/div/div[36]/div/div/div[1]/form/div[5]/div/div[1]/div[1]")
    WebElement NumberofDependants;

    @FindBy(xpath = "/html/body/div[1]/div[2]/div/div[38]/div/div/div[2]/a/div")
    WebElement number1;

  //  @FindBy("personalInfo_MaritalStatus")
    WebElement MaritalStatus;



}
