package pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.WebElement;


public class LoginPage extends PageBase {
    public LoginPage(WebDriver driver)
    {
        super(driver);
     }


    @FindBy(xpath = "/html//div[@id='app']/div[@class='application--wrap']/div[@class='container']/div[@id='app']/div[@class='application--wrap']/div[@class='container']/div[1]/div[2]/div[@class='container py-0']/div[3]/div/form[@class='v-form']//input[@type='email']")
    WebElement UserLoginEmail;

    @FindBy(xpath ="/html//div[@id='app']/div[@class='application--wrap']/div[@class='container']/div[@id='app']/div[@class='application--wrap']/div[@class='container']/div[1]/div[2]/div[@class='container py-0']/div[3]/div/form[@class='v-form']//input[@type='password']")
    WebElement UserLoginPassword;


    @FindBy(xpath = "/html//div[@id='app']/div[@class='application--wrap']/div[@class='container']/div[@id='app']/div[@class='application--wrap']/div[@class='container']/div[1]/div[2]/div[@class='container py-0']/div[4]/div[@class='container py-0']/div[@class='layout row']/div[3]/button[@type='button']/div[@class='v-btn__content']")
    WebElement ClickButton;

    public void UserLoginPage(String EmailLogin, String PasswordLogin)
    {
        SetElement(UserLoginEmail, EmailLogin);

        SetElement(UserLoginPassword, PasswordLogin);

        clickOn(ClickButton);
    }

    public void Scrollog()
    {
        ScrolltoButtom();
    }


}
