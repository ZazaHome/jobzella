package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Course extends PageBase {
    public Course(WebDriver driver)
    {
        super(driver);
        Jse = (JavascriptExecutor) driver;
    }

    @FindBy(id = "navbarDropdown")
    WebElement coursePostingDrpDown;

    @FindBy(linkText = "/courses/add")
    WebElement postCourse;

    @FindBy(id = "title")
    WebElement TitleTxt;

    @FindBy(id = "mapAutoText")
    WebElement mapTxt;

    @FindBy(id = "language")
    WebElement languageDropdown;
    @FindBy(xpath = "/html/body/div[1]/div[2]/div/div[2]/div[9]/div/div/div[6]/a/div")
    WebElement chooseArabic;

    @FindBy(id = "courseField")
    WebElement coursefiledDropdown;
    @FindBy(id = "/html/body/div[1]/div[2]/div/div[2]/div[8]/div/div/div[6]/a/div")
    WebElement chooseArts;

    @FindBy(id = "courseType")
    WebElement coursetypeDropDown;
    @FindBy(xpath = "/html/body/div[1]/div[2]/div/div[2]/div[7]/div/div/div[4]/a/div")
    WebElement chooseshortcourse;

    @FindBy(id = "courseDesignedFor")
    WebElement courseDesignedDropDown;
    @FindBy(id = "/html/body/div[1]/div[2]/div/div[2]/div[6]/div/div/div[1]/a/div")
    WebElement chooseindividuals;

    @FindBy(id = "courseAccredited")
    WebElement accreditedDropDown;
    @FindBy(xpath = "/html/body/div[1]/div[2]/div/div[2]/div[5]/div/div/div[2]/a/div")
    WebElement chooseCertified;


    public void addCourses(String title, String Map) throws Exception
    {
        clickOn(coursePostingDrpDown);
        clickOn(postCourse);
        SetElement(TitleTxt, title);
        SetElement(mapTxt, Map);
        Thread.sleep(2000);
        mapTxt.sendKeys(Keys.ENTER);
        Thread.sleep(2000);
        clickOn(languageDropdown);
        Thread.sleep(2000);
        clickOn(chooseArabic);
        clickOn(coursefiledDropdown);
        clickOn(chooseArts);
        Thread.sleep(2000);
        clickOn(coursetypeDropDown);
        Thread.sleep(2000);
        clickOn(chooseshortcourse);
        clickOn(courseDesignedDropDown);
        Thread.sleep(2000);
        clickOn(chooseindividuals);
        clickOn(accreditedDropDown);
        Thread.sleep(2000);
        clickOn(chooseCertified);

    }


    public void Scroll() {
        ScrolltoButtom();
    }

}

