package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

public class TargetJobs extends PageBase {
    public TargetJobs(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//*[@id=\"targetJob\"]/ul/li/div[1]/div/div/div[2]/button/i")
    WebElement AddnewTargetJob;

    @FindBy(id = "targetJobTitleInput")
    WebElement TargetobTitleTXT;

    @FindBy(id = "targetJobCountryInput")
    WebElement Country;

    @FindBy(xpath = "/html/body/div[1]/div[2]/div/div[10]/div/div/div[2]/a/div")
    WebElement Emirates;

    @FindBy(xpath = "/html//input[@id='targetJobCityInput']")
    WebElement City;

    @FindBy(xpath = "/html/body/div[1]/div[2]/div/div[9]/div/div/div[3]/a/div")
    WebElement Dubai;

    @FindBy(id = "targetJobExpectedSalaryInput")
    WebElement expectedSallary;

    @FindBy(id = "targetJobCurrencyNameInput")
    WebElement CurrencySelect;

    @FindBy(xpath = "/html/body/div[1]/div[2]/div/div[8]/div/div/div[1]/a/div")
    WebElement AED;

    @FindBy(xpath = "//*[@id=\"targetJobSaveBtn\"]")
    WebElement SaveButton;

    public void TargetJobs ( String name, String sallary) throws InterruptedException
    {
        clickOn(AddnewTargetJob);

        SetElement(TargetobTitleTXT, name);

        clickOn(Country);
        Thread.sleep(2000);
        clickOn(Emirates);

        clickOn(City);
        Thread.sleep(2000);
        clickOn(Dubai);

        SetElement(expectedSallary, sallary);

        clickOn(CurrencySelect);
        Thread.sleep(2000);
        clickOn(AED);

        clickOn(SaveButton);
    }

}



