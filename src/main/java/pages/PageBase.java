package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class PageBase
{
    protected WebDriver driver;
    public  Actions action;
    public JavascriptExecutor Jse;
    public Select select;

    //Const
    public PageBase(WebDriver driver)
    {
        PageFactory.initElements (driver, this);
    }

    //Method For ClickButton
    protected static void clickOn (WebElement Button)
    {
      Button.click();
    }

    //Method For SetElements
    protected static void SetElement (WebElement Element , String Value )
    {
        Element.sendKeys(Value);
    }


    //Method For ScrollDown
    public void ScrolltoButtom()
    {
      Jse.executeAsyncScript("scrollBy(0,2500)");
    }

    //Method For Clear Element
    public void clearTXT ( WebElement element)
    {
        element.clear();
    }




}
