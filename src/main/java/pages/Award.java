package pages;

import org.apache.wicket.util.upload.FileUpload;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Award extends PageBase
{
    public Award(WebDriver driver)
    {
        super(driver);
    }


    @FindBy(xpath = "/html/body/div[1]/div[2]/div/div[69]/main/div/div[2]/div/div[2]/div[2]/div/div[7]/ul/li/div[1]/div/div/div[2]/button/div/i")
    WebElement AddAwards;

    @FindBy(id = "awardsTitleInput")
    WebElement AwardsTitle;

    @FindBy(id = "awardsIssuerInput")
    WebElement AwardsIssuer;

    @FindBy(id = "awardsDescriptionInput")
    WebElement Description;

    @FindBy(id = "awardsDateAwardedInput")
    WebElement Calender;

    @FindBy(xpath = "/html/body/div[1]/div[2]/div/div[1]/div/div/div/div[1]")
    WebElement Left;

    @FindBy(xpath = "//*[@id=\"awardsSaveBtn\"]")
    WebElement SaveButton;

    public void UserAddAwards(String title1, String issue2, String description3) throws InterruptedException
    {
        clickOn(AddAwards);

        SetElement(AwardsTitle, title1);

        SetElement(AwardsIssuer, issue2);

        SetElement(Description, description3);

        clickOn(Calender);
        Thread.sleep(1000);
        clickOn(Left);

        clickOn(SaveButton);

    }

//    public void uploadFile()
//    {
//         String ImageName = "zaza.jpg";
//         String ImagePath = System.getProperty("user.dir") + "/Uploads/" + ImageName;
//        WebElement FileUploader = driver.findElement(By.id("awardsSelectImageInput"));
//        FileUploader.sendKeys(ImagePath);
//        clickOn(SaveButton);
//    }


}



