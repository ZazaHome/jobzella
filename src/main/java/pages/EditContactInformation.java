package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EditContactInformation extends PageBase
{
    public EditContactInformation(WebDriver driver)
    {
        super(driver);
    }

    @FindBy(xpath = "/html//main[@id='userInfo']//button[@type='button']/div[@class='v-btn__content']")
    WebElement OpenContactInfo;

   @FindBy(id = "contactInfoMobilePhoneInput")
       WebElement MobilePhoneTxt;

    @FindBy(id = "contactInfoWebSiteInput")
    WebElement WebsiteTxt;

    @FindBy(id = "contactInfoFaceBookInput")
    WebElement FacebookTxt;

    @FindBy(id = "contactInfoTwitterInput")
    WebElement TwitterTxt;

    @FindBy(id = "contactInfoLinkedInInput")
    WebElement LinkedinTxt;


    @FindBy(id = "//*[@id=\"contactInfoSaveBtn\"]")
    WebElement SaveButton;


    public void ContactInfo (String Phone ,String Website , String Facebook ,String Twitter ,String Linkedin)
    {
        clickOn(OpenContactInfo);
        SetElement(MobilePhoneTxt,Phone);
        SetElement(WebsiteTxt,Website);
        SetElement(FacebookTxt,Facebook);
        SetElement(TwitterTxt,Twitter);
        SetElement(LinkedinTxt,Linkedin);
        clickOn(SaveButton);

    }
}
