package pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends PageBase {
    public HomePage(WebDriver driver)
    {
        super(driver);

    }

//    @FindBy(css = ".register-btn")
//    WebElement CreatnewAccount;

    @FindBy(xpath = "/html/body/div[1]/div[2]/div/div[4]/nav/div/div/div/button[2]")
    WebElement loginPage;

//    public void OpenRegisterationPage()
//    {
//        clickOn(CreatnewAccount);
//    }

    public void OpenLoginpage()
    {
        clickOn(loginPage);
    }

}


