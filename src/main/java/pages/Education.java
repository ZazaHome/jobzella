package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Education extends PageBase {
    public Education(WebDriver driver)
    {
        super(driver);
    }

    @FindBy(xpath = "/html/body/div[1]/div[2]/div/div[67]/main/div/div[2]/div/div[2]/div[2]/div/div[6]/ul/li/div[1]/div/div/div[2]/button")
    WebElement AddEducationButton;

    @FindBy(id = "educationSchoolUniversityInput")
    WebElement SchoolTxt;

    @FindBy(id = "educationDegreeInput")
    WebElement DgreeTxt;

    @FindBy(id = "educationGradeInput")
    WebElement GradeTxt;

    @FindBy(id="educationFieldOfStudyInput")
    WebElement FildOfStudyTxt;

    @FindBy(xpath = "//*[@id=\"educationSaveBtn\"]")
    WebElement SaveButton;

public void UserEducation(String School , String degree , String grade , String Filedofstudy )
{
   clickOn(AddEducationButton);
   SetElement(SchoolTxt, School);
   SetElement(DgreeTxt,degree);
   SetElement(GradeTxt,grade);
   SetElement(FildOfStudyTxt,Filedofstudy);
   clickOn(SaveButton);
}


public void Scroll()
{
    ScrolltoButtom();
}

}
