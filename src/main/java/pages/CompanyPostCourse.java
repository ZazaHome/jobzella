package pages;


import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CompanyPostCourse extends PageBase
{

    public CompanyPostCourse(WebDriver driver)
    {
        super(driver);
    }

    @FindBy(id = "navbarDropdown")
    WebElement CourspostingDropdwn;

    @FindBy(xpath = "//*[@id=\"companySocialBarLink\"]")
    WebElement PostCourse;

    @FindBy(id = "title")
    WebElement TitleTxt;

    @FindBy(id = "mapAutoText")
    WebElement MapTxt;

    @FindBy(id = "language")
    WebElement LanguageButtun;
    @FindBy(id = "/html/body/div[1]/div[2]/div/div[2]/div[8]/div/div/div[6]/a/div/div")
    WebElement Arabic;

    @FindBy(id = "courseField")
    WebElement CourseDrpdwn;
    @FindBy(xpath = "/html/body/div[1]/div[2]/div/div[2]/div[7]/div/div/div[7]/a/div/div")
    WebElement Art;


    @FindBy(id = "courseType")
    WebElement coursetypeDrpdwn;
    @FindBy(xpath = "/html/body/div[1]/div[2]/div/div[2]/div[6]/div/div/div[1]/a/div/div")
    WebElement CertificatesDiplomas;

    @FindBy(id = "courseDesignedFor")
    WebElement DesignDrpdwn;
    @FindBy(xpath = "/html/body/div[1]/div[2]/div/div[2]/div[5]/div/div/div[2]/a/div/div")
    WebElement Businesse;

    @FindBy(id = "courseAccredited")
    WebElement AccreditedDrpdwn;
    @FindBy(xpath = "/html/body/div[1]/div[2]/div/div[2]/div[4]/div/div/div[1]/a/div/div")
    WebElement Accredited;

    @FindBy(id = "courseStartDate")
    WebElement openDatePaickerStartDate;
    @FindBy(xpath = "/html/body/div[1]/div[2]/div/div[2]/div[1]/div/div/div/div[2]/table/tbody/tr[1]/td[4]")
    WebElement StartDate4;

    @FindBy(id = "courseDeadlineDate")
    WebElement OpenDatePaickerEndDate;
    @FindBy(id = "/html/body/div[1]/div[2]/div/div[2]/div[1]/div/div/div/div[2]/table/tbody/tr[2]/td[4]/button")
    WebElement EndDate8;

    @FindBy(id = "courseDuration")
    WebElement DurationTxt;

    @FindBy(id = "courseDurationType")
    WebElement PerDrpdwn;
    @FindBy(xpath = "/html/body/div[1]/div[2]/div/div[2]/div[4]/div/div/div[2]/a/div")
    WebElement ChooseWeeks;

    @FindBy(id = "courseWorkOverload")
    WebElement WorkOverloadTxt;

    @FindBy(id = "courseDescription")
    WebElement DescriptionTxt;

    @FindBy(id = "newCourseFixedPriceBtn")
    WebElement FixidPriceButtun;

    @FindBy(id = "coursePrice")
    WebElement PriceTxt;

    @FindBy(id = "courseSelectPrice")
    WebElement CurrencyDrpdwn;
    @FindBy(xpath = "/html/body/div[1]/div[2]/div/div[2]/div[1]/div/div/div[1]/a/div")
    WebElement chooseEmiratiDirham;

    @FindBy(id = "courseSelectSpecialPrice")
    WebElement SpecialPriceDrpdwn;
    @FindBy(xpath = "/html/body/div[1]/div[2]/div/div[2]/div[4]/div/div/div[2]/a/div")
    WebElement chooseNo;

    @FindBy(id = "courseSpecialOffer")
    WebElement SpesialOffer;
    @FindBy(id = "courseDiscount")
    WebElement CourseDiscountTxt;

    @FindBy(xpath = "//*[@id=\"continueBtn\"]")
    WebElement ContinueButton;

    public void CoursePosting(String Languages, String map, String Overload, String Price, String Number)
    {
        clickOn(CourspostingDropdwn);
        clickOn(PostCourse);
        SetElement(TitleTxt, Languages);
        SetElement(MapTxt, map);
        MapTxt.sendKeys(Keys.ENTER);
        clickOn(LanguageButtun);
        clickOn(Arabic);
        clickOn(CourseDrpdwn);
        //Thread.sleep(1000);
        clickOn(Art);
        clickOn(coursetypeDrpdwn);
        clickOn(CertificatesDiplomas);
        clickOn(DesignDrpdwn);
        clickOn(Businesse);
        clickOn(AccreditedDrpdwn);
        clickOn(Accredited);
        clickOn(openDatePaickerStartDate);
        clickOn(StartDate4);
        clickOn(OpenDatePaickerEndDate);
        clickOn(EndDate8);
        clickOn(DurationTxt);
        clickOn(PerDrpdwn);
        clickOn(ChooseWeeks);
        SetElement(WorkOverloadTxt, Overload);
        clickOn(DescriptionTxt);
        clickOn(FixidPriceButtun);
        SetElement(PriceTxt, Price);
        clickOn(CurrencyDrpdwn);
        clickOn(chooseEmiratiDirham);
        clickOn(SpecialPriceDrpdwn);
        clickOn(chooseNo);
        clickOn(SpesialOffer);
        SetElement(CourseDiscountTxt, Number);
        clickOn(ContinueButton);
    }

    public void Scroll()
    {
        ScrolltoButtom();
    }


}
