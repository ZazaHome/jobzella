package pages;

import javafx.scene.control.DatePicker;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.List;

public class Projects extends PageBase
{
    public Projects(WebDriver driver) {
        super(driver);

    }

    @FindBy(xpath = "//div[@id='projects']/ul/li[@class='v-expansion-panel__container']/div[@class='v-expansion-panel__header']/div/div/div[2]/button/i[.='add']")
    WebElement AddProjects;
    @FindBy(id = "projectsProjectNameInput")
    WebElement Projectname;
    @FindBy(id = "projectsProjectUrlInput")
    WebElement ProjectURL;
    @FindBy(id = "projectsDescriptionInput")
    WebElement ProjectDescription;
    @FindBy(id = "editProjectsTimePeriodFromInput1")
    WebElement ProjectFrom;
    @FindBy(xpath = "/html//form[@id='addProjectForm']/div[@class='container']/div[@class='layout row']/div[4]//input[@required='required']")
    WebElement ProjectTo;
    @FindBy(xpath = "//button[@id='addProjectSaveBtn']/div[@class='v-btn__content']")
    WebElement SaveButton;

    public void UserAddProject(String Projname, String ProjURL, String ProjDescription) {
        clickOn(AddProjects);
        SetElement(Projectname, Projname);
        SetElement(ProjectURL, ProjURL);
        SetElement(ProjectDescription, ProjDescription);
//        clickOn(ProjectFrom);
//        JavascriptExecutor js = (JavascriptExecutor) driver;
//        js.executeScript("document.getElementByxpath('/html//div[@id='editProjectsDatePickerInput']/div//table/tbody/tr[1]/td[2]/button[@type='button']/div[@class='v-btn__content']'.value='2019-02'");
//        //clickOn(ProjectTo);
//        JavascriptExecutor js2 = (JavascriptExecutor) driver;
//        js2.executeScript("document.quarySelector('#app > div:nth-of-type(1).tr:nth-of-type(1).td:nth-of-type(3).v-btn__content'.value='2019-03'");
//        js2.executeScript("document.getElementById('ProjectTo'.value='1999-22'");
//        clickOn(SaveButton);
    }



}
