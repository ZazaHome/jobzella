package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;

public class Experiance extends PageBase {
    public Experiance(WebDriver driver) {
        super(driver);
        Jse = (JavascriptExecutor) driver;
    }

    @FindBy(xpath = "//*[@id=\"experiences\"]/ul/li/div[1]/div/div/div[2]/button/i")
    WebElement AddExperianceButton;

    @FindBy(id = "experiencesTitleInput")
    WebElement TitleTxt;

    @FindBy(id = "experiencesCompanyNameInput")
    WebElement CompanynameTxt;

    @FindBy(xpath = "/html//input[@id='experiencesCompanyIndustryInput']")
    WebElement CompanyIndustrySelector;

@FindBy(xpath = "/html/body/div[1]/div[2]/div/div[32]/div/div/div[1]/a/div")
WebElement Accounting_Auditing;

    @FindBy(xpath = "/html//input[@id='experiencesJobRoleInput']")
    WebElement JobRoleSelector;

    @FindBy(xpath = "/html/body/div[1]/div[2]/div/div[31]/div/div/div[17]/a/div")
    WebElement Information_Technology;

    @FindBy(id = "experiencesLocationInput")
    WebElement LocationTXT;

    @FindBy(id = "experiencesFromDateInput")
    WebElement SelectFrom;

@FindBy(xpath = "/html//div[@id='experiencesDatePickerFromInput']/div//table/tbody/tr[2]/td[3]/button[@type='button']/div[@class='v-btn__content']")
WebElement Select13;

    @FindBy(xpath = "/html//input[@id='experiencesToDateInput']")
    WebElement SelectTo;

    @FindBy(xpath = "/html/body/div[1]/div[2]/div/div[2]/div/div/div/div[2]/table/tbody/tr[4]/td[3]")
    WebElement Select19;

    @FindBy(id = "experiencesDescriptionInput")
    WebElement DescriptionTxt;

    @FindBy(id = "//*[@id=\"addExperienceSaveBtn\"]/div")
    WebElement SaveButton;

    public void UserAddExperiance(String title, String CompanyName, String Location, String description) throws InterruptedException
    {

        clickOn(AddExperianceButton);

        SetElement(TitleTxt, title);

        SetElement(CompanynameTxt, CompanyName);

        clickOn(CompanyIndustrySelector);

        clickOn(Accounting_Auditing);

        Thread.sleep(1000);

        clickOn(JobRoleSelector);

        clickOn(Information_Technology);

        SetElement(LocationTXT, Location);


        clickOn(SelectFrom);
        Thread.sleep(3000);
        clickOn(Select13);

        clickOn(SelectTo);
        Thread.sleep(1000);
        clickOn(Select19);


        SetElement(DescriptionTxt, description);

        clickOn(SaveButton);
    }
}